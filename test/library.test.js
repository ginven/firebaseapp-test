
  var webdriver = require('selenium-webdriver'),
    By = webdriver.By,
    until = webdriver.until;
   var { describe, it, after, before } = require('selenium-webdriver/testing');
   var Page = require('../lib/home_page');
   var chai = require('chai');
   var chaiAsPromised = require('chai-as-promised');
   var should = chai.should();
   var page;
   chai.use(chaiAsPromised);

    // assert = require('assert'),

    describe('library app scenarios', function(){
    	this.timeout(50000);
    	beforeEach(function(){
		page = new Page();
		page.visit('https://library-app.firebaseapp.com/');
    	});

    	afterEach(function(){
			page.quit();
    	});

    	it('Typing valid email changes button opacity to 1', function(){
    		var btn = page.requestBtn();
    		btn.opacity.should.eventually.equal('1');
    	});

    	it('Typing a valid email enables request button', function(){
    		var btn = page.requestBtn();
    		btn.state.should.eventually.be.true;
    	});

    	it('Clicking request information triggers a confirmation allert', function(){
    		var alert = page.alertSuccess();
    		alert.should.eventually.contain('Thank you');
    	});
    })



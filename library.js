var webdriver = require('selenium-webdriver'),
 { describe, it, after, before } = require('selenium-webdriver/testing'),
    By = webdriver.By,
    until = webdriver.until;

    describe('library app scenarios', function(){
    	beforeEach(function(){
		    var driver = new webdriver.Builder().forBrowser('chrome').build();
			driver.get('http://library-app.firebaseapp.com');
    	});

    	afterEach(function(){
			driver.quit();
    	});

    	it('Changes Button opacity when inserting an email', function(){
		var submitBtn = driver.findElement(By.css('.btn-lg'));
		driver.findElement(By.css('input')).sendKeys('user@fakemail.com');

		driver.wait(function(){
		return submitBtn.getCssValue('opacity').then(function(result){
			return result == 1;
			});
		}, 5000)
    	});

    	it('works with mocha', function(){
    		var submitBtn = driver.findElement(By.css('.btn-lg'));
    		driver.findElement(By.css('input')).sendKeys('usercom');
			submitBtn.click();
			driver.wait(until.elementLocated(By.css('.alert-success')), 5000);
			driver.findElement(By.css('.alert-success')).getText().then(function(txt){
				console.log("Success message is " + txt)
			})
    	});

    	it('works with mocha', function(){
    		driver.findElement(By.css('navbar')).getText().then(function(txt){
    			console.log(txt);
    		})
    	});
    })



	// driver.manage().timeouts().implicitlyWait(5000);







// driver.findElement(By.css('input'));
//     driver.findElement(By.css('.btn-lg'));
//     driver.findElements(By.css('nav li')).then(function(elements){
//     	elements.map(function(el){
//     		el.getText().then(function(txt){
//     			console.log("Text of the element is " + txt);
//     		})
//     	})
//     });